app.controller('homeCtrl', function($scope,$rootScope,$window,$state, $http, $document,$timeout) { 

        $scope.pbxArray = $window.localStorage.getItem('pbx');

        if ($scope.pbxArray == null) {
            $state.go('login');
        }

        /*$scope.oSipStack;
        $scope.oSipSessionRegister;
        $scope.oSipSessionCall;
        $scope.oConfigCall;*/
        
        $scope.enableCallBtn = true;
        $scope.alerts = [];
        $scope.dialPadVis = true;
        $scope.phoneRingView = false;
        $scope.phoneInCallView = false;

        $scope.timerRunning = false;
 
        $scope.startCallTimer = function (){
            $scope.$broadcast('timer-start');
            $scope.timerRunning = true;
        };

        $scope.stopCallTimer = function (){
            $scope.$broadcast('timer-stop');
            $scope.timerRunning = false;
        };

        
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };


        $scope.sipHangUp = function() {
            if ($scope.oSipSessionCall) {
                console.log('<i>Terminating the call...</i>');
                $scope.oSipSessionCall.hangup({ events_listener: { events: '*', listener:  onSipEventSession } });
                $scope.$apply();
            }
        }

         $scope.sipSendDTMF = function(c) {
            if ( $scope.oSipSessionCall && c) {
                if ( $scope.oSipSessionCall.dtmf(c) == 0) {
                    try { $scope.dtmfTone.play(); } catch (e) { }
                }
            }
        }

         $scope.startRingTone = function() {
            try { $document[0].getElementById('ringtone').play(); }
            catch (e) { }
        }

         $scope.stopRingTone = function() {
            try {  $document[0].getElementById('ringtone').pause(); }
            catch (e) { }
        }

         $scope.startRingbackTone = function() {
            try {  $scope.ringbacktone.play(); }
            catch (e) { }
        }

         $scope.stopRingbackTone = function() {
            try {  $scope.ringbacktone.pause(); }
            catch (e) { }
        }

        $scope.acceptInCall = function(){
            $scope.phoneRingView = false;
            $scope.phoneInCallView = true;

            $scope.startCallTimer();
            
            $scope.oSipSessionCall.accept();
            $scope.$apply;
        }

        $scope.sipHangUp = function() {
            if ($scope.oSipSessionCall) {
                
                $scope.dialPadVis = true;
                $scope.phoneRingView = false;
                $scope.phoneInCallView = false;

                $scope.stopCallTimer();
                //Play drop call

                console.log('Terminating the call...');
                $scope.oSipSessionCall.hangup({ events_listener: { events: '*', listener: onSipEventSession } });
                $scope.$apply;
            }
        }

        $scope.sipToggleMute = function() {
            if ($scope.oSipSessionCall) {
                var i_ret;
                var bMute = !$scope.oSipSessionCall.bMute;
                i_ret = $scope.oSipSessionCall.mute('audio'/*could be 'video'*/, bMute);
                if (i_ret != 0) {
                    alert('Mute / Unmute failed');
                    return;
                }
                $scope.oSipSessionCall.bMute = bMute;
            }
        }


        $scope.uiCallTerminated = function(e){
            console.log(e);

            $scope.stopRingbackTone();
            $scope.stopRingTone();
            $scope.dialPadVis = true;
            $scope.phoneRingView = false;
            $scope.phoneInCallView = false;

            $scope.stopCallTimer();

            $scope.oSipStack.stop();
            $scope.sipRegister();

            $scope.$apply();
            
        }

       $scope.sipRegister = function() {
        try {
            
             $scope.oSipStack = new SIPml.Stack({
                realm: 'pbx.compassholding.net',
                impi: '998104',
                impu: 'sip:998104@pbx.compassholding.net:5060',
                password: '2ebed6cb4673e9d622502e7be2506d77',
                display_name: 'Milos Petrovic',
                websocket_proxy_url: 'wss://pbx.compassholding.net:8089/ws',
                outbound_proxy_url: 'udp:///pbx.compassholding.net:5060',
                ice_servers: [{"url":"stun:stun.l.google.com:19302"}],
                enable_rtcweb_breaker: false,
                events_listener: { events: '*', listener:  $scope.onSipEventStack },
                enable_early_ims: false, 
                enable_media_stream_cache: false,
                sip_headers: [
                        { name: 'User-Agent', value: 'CompassCentral/Desk1.0 v1' },
                        { name: 'Organization', value: 'Compass Holding LLC' }
                ]
            }
            );
            if ( $scope.oSipStack.start() != 0) {
                console.log('Failed to start the SIP stack');
            }
            else return;
        }
        catch (e) {
            console.log(e);
        }
        
    }     

    function onSipEventSession (e) {
        tsk_utils_log_info('==session event = ' + e.type);

        switch (e.type) {
            case 'connecting': case 'connected':
                {
                    var bConnected = (e.type == 'connected');
                    if (e.session == $scope.oSipSessionRegister) {
                        console.log(e);
                    }
                    else if (e.session == $scope.oSipSessionCall) {
                        
                        if (window.btnBFCP) window.btnBFCP.disabled = false;

                        if (bConnected) {
                            $scope.stopRingbackTone();
                            $scope.stopRingTone();

                            if ($scope.oNotifICall) {
                                $scope.oNotifICall.cancel();
                                $scope.oNotifICall = null;
                            }
                        }

                        console.log("<i>" + e.description + "</i>");

                        if (SIPml.isWebRtc4AllSupported()) { // IE don't provide stream callback
                            uiVideoDisplayEvent(false, true);
                            uiVideoDisplayEvent(true, true);
                        }
                    }
                    break;
                } // 'connecting' | 'connected'
            case 'terminating': case 'terminated':
                {
                    if (e.session == $scope.oSipSessionRegister) {
                        
                        $scope.oSipSessionCall = null;
                        $scope.oSipSessionRegister = null;

                        $scope.$apply;
                    }
                    else if (e.session == $scope.oSipSessionCall) {
                        $scope.uiCallTerminated(e.description);
                    }
                    break;
                } // 'terminating' | 'terminated'

            
            case 'i_ect_new_call':
                {
                    $scope.oSipSessionTransferCall = e.session;
                    console.log('mile');
                    break;
                }

            case 'i_ao_request':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        var iSipResponseCode = e.getSipResponseCode();
                        if (iSipResponseCode == 180 || iSipResponseCode == 183) {
                            $scope.startRingbackTone();
                            console.log('<i>Remote ringing...</i>');
                        }
                    }
                    break;
                }

            case 'm_early_media':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        $scope.stopRingbackTone();
                        $scope.stopRingTone();
                        console.log('<i>Early media started</i>');
                    }
                    break;
                }

            case 'm_local_hold_ok':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        if ($scope.oSipSessionCall.bTransfering) {
                            $scope.oSipSessionCall.bTransfering = false;
                            // this.AVSession.TransferCall(this.transferUri);
                        }
                        console.log('<i>Call placed on hold</i>');
                        $scope.oSipSessionCall.bHeld = true;
                    }
                    break;
                }
            case 'm_local_hold_nok':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        $scope.oSipSessionCall.bTransfering = false;
                        console.log('<i>Failed to place remote party on hold</i>');
                    }
                    break;
                }
            case 'm_local_resume_ok':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        $scope.oSipSessionCall.bTransfering = false;
                        console.log('<i>Call taken off hold</i>');
                        

                        if (SIPml.isWebRtc4AllSupported()) { // IE don't provide stream callback yet
                            uiVideoDisplayEvent(false, true);
                            uiVideoDisplayEvent(true, true);
                        }
                    }
                    break;
                }
            case 'm_local_resume_nok':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        console.log('<i>Failed to unhold call</i>');
                    }
                    break;
                }
            case 'm_remote_hold':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        console.log('<i>Placed on hold by remote party</i>');
                    }
                    break;
                }
            case 'm_remote_resume':
                {
                    if (e.session == oSipSessionCall) {
                        txtCallStatus.innerHTML = '<i>Taken off hold by remote party</i>';
                    }
                    break;
                }
            case 'm_bfcp_info':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        console.log('BFCP Info: <i>' + e.description + '</i>');
                    }
                    break;
                }

            case 'o_ect_trying':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        console.log('<i>Call transfer in progress...</i>');
                    }
                    break;
                }
            case 'o_ect_accepted':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        console.log('<i>Call transfer accepted</i>');
                    }
                    break;
                }
            case 'o_ect_completed':
            case 'i_ect_completed':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        console.log('<i>Call transfer completed</i>');
                        if ($scope.oSipSessionTransferCall) {
                            $scope.oSipSessionCall = $scope.oSipSessionTransferCall;
                        }
                        $scope.oSipSessionTransferCall = null;
                    }
                    break;
                }
            case 'o_ect_failed':
            case 'i_ect_failed':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        console.log('<i>Call transfer failed</i>');
                    }
                    break;
                }
            case 'o_ect_notify':
            case 'i_ect_notify':
                {
                    if (e.session == $scope.oSipSessionCall) {
                        console.log("<i>Call Transfer: <b>" + e.getSipResponseCode() + " " + e.description + "</b></i>");
                        if (e.getSipResponseCode() >= 300) {
                            if ($scope.oSipSessionCall.bHeld) {
                                $scope.oSipSessionCall.resume();
                            }
                        }
                    }
                    break;
                }
        }
    }
     

         $scope.onSipEventStack = function(e) {
            //tsk_utils_log_info('==stack event = ' + e.type);
            switch (e.type) {
                case 'started':
                    {
                        
                        try {
                            
                                $scope.oSipSessionRegister = this.newSession('register', {
                                expires: 200,
                                events_listener: { events: '*', listener: onSipEventSession },
                                sip_caps: [
                                            { name: '+g.oma.sip-im', value: null },
                                            //{ name: '+sip.ice' }, // rfc5768: FIXME doesn't work with Polycom TelePresence
                                            { name: '+audio', value: null },
                                            { name: 'language', value: '\"en,fr\"' }
                                ]
                            });
                            $scope.oSipSessionRegister.register();
                        }
                        catch (e) {
                            console.log(e);
                        }
                        break;
                    }
                case 'stopping': case 'stopped': case 'failed_to_start': case 'failed_to_stop':
                    {
                        
                        $scope.stopRingbackTone();
                        $scope.stopRingTone();

                        console.log("<i>Disconnected: <b>" + e.description);
                        break;
                    }

                case 'i_new_call':
                    {
                        if ($scope.oSipSessionCall) {
                            // do not accept the incoming call if we're already 'in call'
                            e.newSession.hangup(); // comment this line for multi-line support
                        }
                        else {
                            $scope.oSipSessionCall = e.newSession;
                            // start listening for events
                            $scope.oSipSessionCall.setConfiguration({audio_remote: $document[0].getElementById('audioRemote'),
                            events_listener: { events: '*', listener: onSipEventSession },
                            sip_caps: [
                                            { name: '+g.oma.sip-im' },
                                            { name: 'language', value: '\"en,fr\"' }
                            ]});
                            
                            $scope.dialPadVis = false;
                            $scope.phoneRingView = true;

                            $scope.startRingTone();

                            Notification.requestPermission().then(function(res){
                                console.log(res);
                                var mile = new Notification('Title', {
                                    body: 'Lorem Ipsum Dolor Sit Amet'
                                  });
                                  console.log(mile);
                            })
                            
                            $scope.phoneRingCaller = ($scope.oSipSessionCall.getRemoteFriendlyName() || 'unknown');
                            $scope.$apply();
                            console.log($scope.oSipSessionCall);
                            //txtCallStatus.innerHTML = "<i>Incoming call from [<b>" + sRemoteNumber + "</b>]</i>";

                        }
                        break;
                    }

                case 'm_permission_requested':
                    {
                        console.log('m_permission_requested')
                        break;
                    }
                case 'm_permission_accepted':
                case 'm_permission_refused':
                    {
                        console.log('m_permission_refused')
                        break;
                    }

                case 'starting': default: break;
            }
        };


      $scope.sipRegister();  
      $scope.dialpadNumber = '';

      $scope.dtmf = function(x){
        $scope.dialpadNumber +=x;
      }









  });