app.controller('loginCtrl', function($scope,$rootScope,$window,$state, $http) { 

    $scope.wrongUserData = false;

    $scope.checkLog = function(email,pass){

      var send = "email="+email+"&password="+pass;
      $http.post('http://pbx.compassholding.net/api/v2/loginApp',send,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).then(function(data){

        $scope.loginResponse = data.data;
    
        if($scope.loginResponse.error){
          $scope.wrongUserData = true;
          $scope.wrongUserDataMessage = $scope.loginResponse.message;
        } else {
          $scope.pbxArray = $window.localStorage.setItem('pbx',JSON.stringify($scope.loginResponse.userData));
          $state.go('home');
        }

      });
  
    }


});