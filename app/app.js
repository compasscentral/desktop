
var app = angular.module('myApp', [
  'ui.router',
  'ngProgress',
  'ngAnimate', 
  'ngSanitize', 
  'ui.bootstrap',
  'timer'
])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state({
      name: 'login',
      url: '/login',
      templateUrl: 'views/login.html',
      controller: "loginCtrl"
    })
    .state({
      name: 'forgot',
      url: '/forgot',
      templateUrl: 'views/forgot.html',
      controller: "loginCtrl"
    })
    .state({
      name: 'home',
      url: '/',
      templateUrl: 'views/home.html',
      controller: "homeCtrl"
    })

    
   $urlRouterProvider.when('', '/');
})
.filter('numberpad', function() {
  return function(input, places) {
    var out = "";
    if (places) {
      var placesLength = parseInt(places, 10);
      var inputLength = input.toString().length;
    
      for (var i = 0; i < (placesLength - inputLength); i++) {
        out = '0' + out;
      }
      out = out + input;
    }
    return out;
  };
});